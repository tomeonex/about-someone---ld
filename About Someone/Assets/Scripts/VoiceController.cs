﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceController : MonoBehaviour
{

    public GameObject player;
    public SpriteRenderer spriteRenderer;

    public Sprite[] sprites;

    private int counter = 0;

    private float time = 0f;
    private float textTime = 2f;
    private bool countdown = true;

    void Start()
    {
        spriteRenderer.sprite = sprites[counter];
    }

    void Update()
    {
        gameObject.transform.position = player.transform.position + new Vector3(-2,-5,8);

        if(countdown){
            player.GetComponent<PlayerMovement>().enabled = false;
            time += Time.deltaTime;
            if(time > 3f){
                spriteRenderer.sprite = sprites[1];
                counter++;
                time = 0f;
                countdown = false;
                player.GetComponent<PlayerMovement>().enabled = true;
            }
        }
    
        if(!countdown){
        time += Time.deltaTime;
        if(time > textTime){
            spriteRenderer.enabled = false;
        }
        }

    }

    private void OnCollisionEnter(Collision other) {
        if(other.transform.tag == "Player"){
            counter++;
            spriteRenderer.sprite = sprites[counter];
            spriteRenderer.enabled = true;
            time = 0f;
        }
    }

        public void ChangeVoice(){

            counter++;
            spriteRenderer.sprite = sprites[counter];
            spriteRenderer.enabled = true;
            time = 0f;
        }
    }

