﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int hp = 2;

    public Animator animator;
    public UnityEngine.AI.NavMeshAgent agent;
    public BoxCollider box; 
    public EnemyMovement movement;

    void Start()
    {
        
    }

    void Update()
    {
        if(hp <= 0){
            movement.enabled = false;
            agent.enabled = false;
            box.enabled = false;
            animator.SetBool("isDead", true);
        }
    }

    private void OnCollisionEnter(Collision other) {
        if(other.gameObject.transform.tag == "Bullet"){
            hp--;
        }
    }
}
