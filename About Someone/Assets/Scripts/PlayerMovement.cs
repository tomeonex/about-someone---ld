﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    public float speed = 5f;

    public GameObject voiceController;   
    public Canvas canvas;

    private Vector3 movement;
    private Vector3 mousePos;

    private Rigidbody rigidbody;
    public Camera camera;
    public Animator animator;

    void Start()
    {
        Cursor.visible = false;
        rigidbody = GetComponent<Rigidbody>();
        canvas.enabled = false;
    }

    void Update()
    {
        mousePos = camera.ScreenToWorldPoint(Input.mousePosition + Vector3.forward * 10f);

        movement.x = Input.GetAxisRaw("Horizontal");
        movement.z = Input.GetAxisRaw("Vertical");

        if(movement.x != 0 || movement.z != 0){
            animator.SetBool("isMoveing", true);
        }
        else animator.SetBool("isMoveing", false);

    }

    private void FixedUpdate() {

        rigidbody.MovePosition(rigidbody.position + movement * speed * Time.fixedDeltaTime);    

        Vector3 lookDir = mousePos - rigidbody.position;
        float angle = Mathf.Atan2(lookDir.z, lookDir.x) * Mathf.Rad2Deg - 90f;

        rigidbody.rotation = Quaternion.Euler(0, -angle,0);
        
    }

    private void OnCollisionEnter(Collision other) {
        if(other.transform.tag == "Enemy"){
            
        Cursor.visible = true;
            canvas.enabled = true;
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other) {
        if(other.transform.tag == "Voice")
            voiceController.GetComponent<VoiceController>().ChangeVoice();

        if(other.transform.tag == "Dog"){
            
        Cursor.visible = true;
            canvas.enabled = true;

        }
    }

}
